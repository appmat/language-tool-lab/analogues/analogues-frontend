import { configureStore } from "@reduxjs/toolkit"

import {userAnalogue} from "../feature/userAnalogue";

export default configureStore({
  reducer:{
    analogue: userAnalogue.reducer
  }
})