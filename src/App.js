import './App.css';
import { Route, Routes } from "react-router-dom";
import MainPage from "./pages/MainPage";
import NewAnalogue from "./pages/NewAnalogue";
import Layout from "./layouts/Layout";


function App() {
  return (
    <Layout>
      <Routes>
        <Route path="/" element={<MainPage/>}/>
        <Route path="/new-analogue" exact element={<NewAnalogue/>}/>
      </Routes>
    </Layout>
  );
}

export default App;
