import { createSlice } from "@reduxjs/toolkit"
const mockSourceData = [
  {
    analogueName: "Название1",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url1",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название2",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url2",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название3",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url1",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название4",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url2",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название5",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url1",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название6",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url2",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название7",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url1",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название8",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url2",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  }
]
export const userAnalogue = createSlice({
  name: "analogueTag",
  initialState: {
    analogue: null
  },
  reducers: {
    analogue: (state, action) => {
      state.analogue = action.payload
    }
  }
})

export const {analogue} = userAnalogue.actions;

export const selectTag  = (state) => state.analogue.analogue

export default userAnalogue.reducer;