import { Input, Button } from 'antd';
import 'antd/dist/antd.css';
import classes from "../components/Pages.module.css"
import {useDispatch} from "react-redux";
import {analogue} from "../feature/userAnalogue";
import {useState} from "react";
import { Tag, Tooltip } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import axios from 'axios'


const NewAnalogue = () => {
  const [analogueName, setName] = useState("");
  const [analogueTag, setTag] = useState("");
  const [analogueURL, setURL] = useState("");
  const [analogueDescription, setDescription] = useState("");

  const { TextArea } = Input;
  const dispatch = useDispatch();
  const handleSubmit = (e) => {
    axios.get('http://localhost:8000/api/hello').then(resp => {

      alert(resp.data);
    });
    // e.preventDefault()

    // dispatch(analogue({
    //   analogueName,
    //   analogueTag,
    //   analogueURL,
    //   analogueDescription
    // }))
  }

  return (
    <div className={classes.form}>
      <form>
        <div className={classes.block}>
          Название
          <Input name="analogueName"
                 placeholder="название"
                 value={analogueName}
                 onChange={(e) => setName(e.target.value)}
          />
        </div>
        <div className={classes.block}>
          Добавьте тэг
          <Input name="analogueTag"
                 placeholder="тэг"
                 value={analogueTag}
                 onChange={(e) => setTag(e.target.value)}
          />
        </div>
        <div className={classes.block}>
          Добавьте ссылку
          <Input name="analogueURL"
                 placeholder="сайт"
                 value={analogueURL}
                 onChange={(e) => setURL(e.target.value)}
          />
        </div>
        <div className={classes.block}>
          Добавьте краткое описание
          <TextArea name="analogueDescription"
                    value={analogueDescription}
                    onChange={(e) => setDescription(e.target.value)}
                    rows={4} />
        </div>
        <div className={classes.button}>
          <Button onClick={(e) => handleSubmit(e)} type="primary">Отправить</Button>
        </div>
      </form>
    </div>
  )
}

export default NewAnalogue