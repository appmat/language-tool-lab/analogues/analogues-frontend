import { Card, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import classes from "../components/Pages.module.css"
import {Link} from "react-router-dom";
import { useState } from "react"
var _ = require('lodash');

const mockSourceData = [
  {
    analogueName: "Название1",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url1",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название2",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url2",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание." +
      "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название3",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url1",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название4",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url2",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название5",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url1",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название6",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url2",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название7",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url1",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  },
  {
    analogueName: "Название8",
    analogueTag: "тэг тэг тэг",
    analogueURL: "url2",
    analogueDescription: "Большое описание пишем большое нормальное описание. Большое описание пишем большое нормальное описание."
  }
]

const getMappedData = (sourceData) => {
  return (
    sourceData.map(analogue => {
      return (
        <div className={classes.card}>
          <Card type="inner" title={analogue.analogueName} extra={<a href="#">сайт</a>}>
            <div>{analogue.analogueTag}</div>
            <div>{analogue.analogueDescription}</div>
          </Card>
        </div>
        )
    })
  )
}

const getSearch = (data, name) => {
  return getMappedData(_.filter(data, elem => elem.analogueName === name))
}
// const handleSearch = () => {
//
// }
const MainPage = () => {
  // const [allData, setData] = useState(true);

  return (
    <div>
      <Input className={classes.searcher}
             placeholder="поиск"
             prefix={<SearchOutlined />}
             // onKeyPress={(e) => e.key === 'Enter' && handleSearch()}
      />
      {getMappedData(mockSourceData)}
      <Card className={classes.card} type="inner" title="">
        <Link to="/new-analogue">
          Добавить аналог
        </Link>
      </Card>
    </div>
  )
}


export default MainPage