# Getting Started with Create React App

Frontend for analogues project

### Usage
Build and run the app:

```bash
npm run
npm start
```

After that open the site
[http://localhost:3000](http://localhost:3000)